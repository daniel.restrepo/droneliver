require "minitest/autorun"
require_relative "../../lib/droneliver/drone"

class TestDrone < Minitest::Test
  def test_change_direction_right_command
    drone = Droneliver::Drone.new
    drone.location.direction = "W"
    drone.change_direction("D")

    assert_equal "N", drone.location.direction
  end

  def test_change_direction_left_command
    drone = Droneliver::Drone.new # default loc 0,0,N
    drone.change_direction("I")

    assert_equal "W", drone.location.direction
  end

  def test_change_position_from_north
    drone = Droneliver::Drone.new # default loc 0,0,N
    drone.change_position

    assert_equal 1, drone.location.y
    assert_equal 0, drone.location.x
  end

  def test_change_position_from_south
    drone = Droneliver::Drone.new
    drone.location.direction = "S"
    drone.change_position

    assert_equal -1, drone.location.y
    assert_equal 0, drone.location.x
  end

  def test_change_position_from_east
    drone = Droneliver::Drone.new
    drone.location.direction = "E"
    drone.change_position

    assert_equal 0, drone.location.y
    assert_equal 1, drone.location.x
  end

  def test_change_position_from_weast
    drone = Droneliver::Drone.new
    drone.location.direction = "W"
    drone.change_position

    assert_equal 0, drone.location.y
    assert_equal -1, drone.location.x
  end

  # This test is an evidence of an error in the statement
  # Stating at 0,0,N and executing AAAAIAA
  # the finish location isn't -2,4,N
  def test_deliver
    drone = Droneliver::Drone.new
    delivery = Droneliver::Delivery.new("AAAAIAA")
    drone.deliver(delivery)

    assert_equal -2, drone.location.x
    assert_equal 4, drone.location.y
    assert_equal "W", drone.location.direction
  end

  # This test is an evidence of an error in the statement
  # Stating at -2,4,N and executing DDDAIAD
  # the finish location isn't -3,3,S
  def test_deliver
    drone = Droneliver::Drone.new
    drone.location.x = -2
    drone.location.y = 4
    drone.location.direction = "N"
    delivery = Droneliver::Delivery.new("DDDAIAD")
    drone.deliver(delivery)

    assert_equal -3, drone.location.x
    assert_equal 3, drone.location.y
    assert_equal "W", drone.location.direction
  end

  # This test is an evidence of an error in the statement
  # Stating at -3,3,S and executing AAIADAD
  # the finish location isn't -4,2,E
  def test_deliver
    drone = Droneliver::Drone.new
    drone.location.x = -3
    drone.location.y = 3
    drone.location.direction = "S"
    delivery = Droneliver::Delivery.new("AAIADAD")
    drone.deliver(delivery)

    assert_equal -2, drone.location.x
    assert_equal 0, drone.location.y
    assert_equal "W", drone.location.direction
  end

  def test_deliver_delivery_status
    drone = Droneliver::Drone.new
    delivery = Droneliver::Delivery.new("AAAAIAA")
    result = drone.deliver(delivery)

    assert_equal "delivered", result.status
  end
end
