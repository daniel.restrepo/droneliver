require "minitest/autorun"
require_relative "../../lib/droneliver/delivery"

class TestDelivery < Minitest::Test
  def test_default_delivery_status
    delivery = Droneliver::Delivery.new("DDDAIAD")
    assert_equal "pending", delivery.status
  end
end
