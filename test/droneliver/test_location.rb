require "minitest/autorun"
require_relative "../../lib/droneliver/location"

class TestLocation < Minitest::Test
  def test_new_location
    loc = Droneliver::Location.new(7, 4, "N")
    assert_equal 7, loc.x
    assert_equal 4, loc.y
    assert_equal "N", loc.direction
  end

  def test_default_location
    loc = Droneliver::Location.new
    assert_equal 0, loc.x
    assert_equal 0, loc.y
    assert_equal "N", loc.direction
  end

  def test_to_report
    result = Droneliver::Location.new.to_report
    assert_equal "(0, 0) dirección Norte", result
  end
end
