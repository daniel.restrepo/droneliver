require "minitest/autorun"
require_relative "../../lib/droneliver/storage"

class TestStorage < Minitest::Test
  def test_load_successful
    result = Droneliver::Storage.load("test/files/", "in01.txt")
    assert_equal ["AAAAIAA", "DDDAIAD", "AAIADAD"], result
  end

  def test_load_fail
    result = Droneliver::Storage.load("test/files/", "fail.txt")
    assert_empty result
  end

  def test_file_names
    result = Droneliver::Storage.file_names("test/files/")
    assert_equal ["in01.txt"], result
  end

  def test_file_names_without_format
    result = Droneliver::Storage.file_names("test/")
    assert_empty result
  end

  def test_file_number
    result = Droneliver::Storage.file_number("in01.txt")
    assert_equal "01", result
  end

  def test_file_number_only_letters
    result = Droneliver::Storage.file_number("in.txt")
    assert_nil result
  end

  def test_report_file_name
    result = Droneliver::Storage.report_file_name("01")
    assert_equal "out01.txt", result
  end
end
