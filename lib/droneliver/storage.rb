module Droneliver
  class Storage

    DEFAULT_PATH = "data/input/"
    DEFAULT_OUTPUT_PATH = "data/output/"
    INPUT_FILE_PATTERN = /in\d+\.txt/

    def self.load(path = DEFAULT_PATH, filename)
      begin
        file_data = File.read(path + filename).split
      rescue Exception => ex
        puts ("[ERROR]: #{ex.message}")
        []
      end
    end

    def self.file_names(path = DEFAULT_PATH)
      begin
        Dir.entries(path).select { |entry| entry =~ INPUT_FILE_PATTERN }
      rescue Exception => ex
        puts ("[ERROR]: #{ex.message}")
        []
      end
    end

    def self.file_number(filename)
      filename[/\d+/]
    end

    def self.report_file_name(file_number)
      "out#{file_number}.txt"
    end

    def self.report(filename, locations)
      begin
        File.open(DEFAULT_OUTPUT_PATH + filename, "w") do |file|
          file.puts "== Reporte de entregas =="
          file.puts locations
        end
      rescue Exception => ex
        puts ("[ERROR]: #{ex.message}")
      end
    end
  end
end
