module Droneliver
  class Location
    attr_accessor :x, :y, :direction

    def initialize(x = 0, y = 0, direction = "N")
      @x = x
      @y = y
      @direction = direction
    end

    def to_report
      es_direction =
        case @direction
        when "N" then"Norte"
        when "S" then "Sur"
        when "W" then "Occidente"
        when "E" then "Este"
        end
      "(#{@x}, #{@y}) dirección #{es_direction}"
    end
  end
end
