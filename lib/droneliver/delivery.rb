module Droneliver
  class Delivery
    attr_accessor :directions, :status

    def initialize(directions)
      @directions = directions
      @status = "pending"
    end
  end
end
