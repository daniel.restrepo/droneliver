require_relative "location"
require_relative "delivery"

module Droneliver
  class Drone
    attr_accessor :location, :capacity

    CARDINAL_POINTS = %w[N E S W]
    DEFAULT_CAPACITY = 3

    def initialize
      @location = Droneliver::Location.new
      @capacity = DEFAULT_CAPACITY
    end

    def change_direction(command)
      cardinal_index = CARDINAL_POINTS.index(@location.direction)

      case command
      when "D"
        cardinal_index = (cardinal_index + 1) % CARDINAL_POINTS.size
        @location.direction = CARDINAL_POINTS[cardinal_index]
      when "I"
        @location.direction = CARDINAL_POINTS[cardinal_index - 1]
      else
        puts "[Error]: Invalid command #{command}"
      end
    end

    def change_position
      case @location.direction
      when "N"
        @location.y += 1
      when "S"
        @location.y -= 1
      when "E"
        @location.x += 1
      when "W"
        @location.x -= 1
      else
        puts "[Error]: Invalid direction #{@location.direction}"
      end
    end

    def deliver(delivery)
      delivery.directions.each_char do |next_step|
        next_step == "A" ? change_position : change_direction(next_step)
      end
      delivery.status = "delivered"
      delivery
    end
  end
end
