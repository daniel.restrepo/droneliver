require_relative "droneliver/drone"
require_relative "droneliver/storage"

module Droneliver

  def self.run
    files = Droneliver::Storage.file_names
    files.each do |name|
      drone = Droneliver::Drone.new
      directions = Droneliver::Storage.load(name)
      locations = []

      if directions.size <= drone.capacity
        directions.each do |direction|
          delivery = drone.deliver(Delivery.new(direction))
          locations << drone.location.to_report if delivery.status == "delivered"
        end
      else
        puts "[Error]: Drone capacity exceeded in file: #{name}"
      end

      number = Droneliver::Storage.file_number(name)
      report_file = Droneliver::Storage.report_file_name(number)
      Droneliver::Storage.report(report_file, locations)
    end
  end
end

Droneliver.run
