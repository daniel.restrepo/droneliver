# Droneliver

## Dependencies
This application require:
- [Ruby 2.7.1](https://www.ruby-lang.org/es/documentation/installation/)

## How to Run Application
```
ruby lib/droneliver.rb
```

## How to Run Tests
```
ruby test/<file_name>.rb
```
